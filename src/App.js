import "./App.css";
import {useState} from 'react'

import Header from "./components/Header"
import Button from "./components/Button"
import Users from "./components/Users"
import AddUser from "./components/AddUser"

function App() {
  const [userlist,setUsers]=useState([])
  const [showAddUser, setShowAddUser]=useState(false)
  const deleteObj =(id)=>{
    console.log('Delete', id)
    setUsers(userlist.filter((row)=>row.id!==id))
  }
  //Toggle Gender
  const toggleGender=(id)=>{
    alert("Working")
    setUsers(userlist.map((row)=>
      row.id ===id && row.gender ==='Male' ? {...row,gender:'Mwanaume'}: row
      //console.log(row)
    ))
    console.log("Translated to Swahili",id)
  }
  //Add User 
  const addAUser =(user)=>{
    console.log(user)
    const id = Math.floor(Math.random() *1000)+1
    const newUser ={id,...user}
    setUsers([...userlist,newUser])
  }
  return (
    <div className="App">
      <Header title="This Page.."/>
      <Button text="Add A user" color="green" onAdd={()=>setShowAddUser(!showAddUser)} showAdd={showAddUser}/>
      {showAddUser && <AddUser onAdd={addAUser}/>  } 
      { userlist.length > 0 ? < Users userlist={userlist} onDelete={deleteObj} onToggle={toggleGender} /> : <p>No tasks</p>}
    </div>
  );
}

export default App;
