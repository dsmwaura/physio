// import Form from "react-bootstrap/Form";
// const options = ["","Male", "Female"];
import { useState } from "react";

const AddUser = ({onAdd}) => {
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [gender, setGender] = useState("");
  const [ip_address, setIPAddr] = useState("");
  //Submit form
  const onSubmit =(e)=>{
      e.preventDefault();
      onAdd({first_name,last_name,email,gender,ip_address})
      //
      setFirstName('')
      setLastName('')
      setEmail('')
      setGender('')
      setIPAddr('')
  }
  return (
    <form className="form" onSubmit={onSubmit}>
      <div className="form-control">
        <input type="text" placeholder="First Name" value={first_name} onChange={(e)=>setFirstName(e.target.value)}></input>
      </div>
      <div className="form-control">
        <input type="text" placeholder="Last Name" value={last_name} onChange={(e)=>setLastName(e.target.value)}></input>
      </div>
      <div className="form-control">
        <input type="email" placeholder="email" value={email} onChange={(e)=>setEmail(e.target.value)}></input>
      </div>
      <div className="form-control">
        <input type="text" placeholder="Gender" value={gender} onChange={(e)=>setGender(e.target.value)}></input>
      </div>
      <div className="form-control">
        <input type="text" placeholder="IP Address" value={ip_address} onChange={(e)=>setIPAddr(e.target.value)}></input>
      </div>

      <input type="submit" value="Save User" className="btn btn-block" />
    </form>
  );
};

export default AddUser;
