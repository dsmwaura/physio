import {FaTimes} from 'react-icons/fa'
const User = ({user, onDelete, onToggle}) => {
  return (
    <div className={`task ${user.gender==='Female' ? 'reminder':''}`} onDoubleClick ={()=>onToggle(user.id)}>
      <h3>{user.first_name}<small>{user.last_name}</small> <FaTimes style={{color:'red',cursor:'pointer'}} onClick={()=>onDelete(user.id)}/></h3>
      <p>{user.ip_address}</p>
      <p>{user.gender}</p>
    </div>
  );
};

export default User;
