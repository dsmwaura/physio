import User from './User'
const Users = ({userlist, onDelete, onToggle}) => {
    
  return (
      <>
     {
        //  userlist.map((usr)=>(<h3 key={usr.id}>{usr.last_name}</h3>))

        userlist.map((usr)=>(<User key={usr.id} user={usr} onDelete={onDelete} onToggle={onToggle}/>))
     }
      </>
  );
};

export default Users;
